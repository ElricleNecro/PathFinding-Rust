extern crate argparse;
extern crate path_finder;

use argparse::{ArgumentParser,Store};
use path_finder::{breadth_first_search,MapGeometry};

// A trait allowing us to overload [] (index) operator in a non mutable context (IndexMut for a
// mutable context):
use std::ops::Index;

use std::io;
use std::io::prelude::*;
use std::fs::File;
use std::path::Path;
use std::string::String;

/// Structure representing a squared map.
struct SquareGeometry {
    /// map width
    map_w: usize,
    /// map height
    map_h: usize,
    /// flatten map size
    length: usize,

    /// the map itself, flatten
    map: Vec<u8>,

    /// symbols representing a non-passable tile.
    blocked:u8,
}

impl MapGeometry for SquareGeometry {
    /// `contain` test if the coordinate (x, y) are inside the map. It return true if yes.
    /// # Examples
    /// ```
    /// use std::path::Path;
    /// let map = from_file(Path::new("map_test.txt"));
    /// assert!(map.contain(0, 0));
    /// ```
    fn contain(&self, x:isize, y:isize) -> bool {
        if (x < 0 || x >= self.map_w as isize) || (y < 0 || y >= self.map_h as isize) {
            return false;
        }

        let pos = self.to_flatten_idx(x, y);

        if pos >= self.length {
            return false;
        }

        if self.map[pos] == self.blocked {
            return false;
        }

        true
    }

    /// Transform the coordinate (x, y) to a flattened index.
    /// # Examples
    /// ```
    /// use std::path::Path;
    /// let map = from_file(Path::new("map_test.txt"));
    /// let flat_idx = map.to_flatten_idx(0, 1);
    /// assert_eq!(1, flat_idx);
    /// ```
    fn to_flatten_idx(&self, x:isize, y:isize) -> usize {
        self.map_w * y as usize + x as usize
    }

    /// Convert a flatten'd index to a couple of coordinate (x, y).
    /// # Examples
    /// ```
    /// use std::path::Path;
    /// let map = from_file(Path::new("map_test.txt"));
    /// let idx = map.from_flatten_idx(1);
    /// assert_eq!((0, 1), idx);
    /// ```
    fn from_flatten_idx(&self, idx:usize) -> (isize, isize) {
        ((idx % self.map_w) as isize, (idx / self.map_w) as isize)
    }

    /// return the direction of the surrounding neighbors.
    /// # Examples
    /// ```
    /// use std::path::Path;
    /// let map = from_file(Path::new("map_test.txt"));
    /// assert_eq!([(1,0), (0, 1), (-1, 0), (0, -1)], map.neighbors_direction());
    /// ```
    fn neighbors_direction(&self) -> Vec<(isize,isize)> {
        vec![
            (1,0),
            (0,1),
            (-1,0),
            (0,-1),
        ]
    }


    /// return the size of the map.
    /// # Examples
    /// ```
    /// use std::path::Path;
    /// let map = from_file(Path::new("map_test.txt"));
    /// assert_eq!(50, map.len());
    /// ```
    fn len(&self) -> usize {
        self.length
    }
}

impl Index<usize> for SquareGeometry {
    type Output = u8;

    /// Implementing index, to be able to use it as an array.
    fn index(&self, idx:usize) -> &u8 {
        &self.map[idx]
    }
}

/// Open a file and set a buffered reader to easily read lines.
fn lines_from_file<P>(filename: P) -> Result<io::Lines<io::BufReader<File>>, io::Error> where P: AsRef<Path> {
    let file = try!(File::open(filename));
    Ok(io::BufReader::new(file).lines())
}

/// Macro allowing to read inside a string, C-scanf way!
///
/// #Examples
/// ```
/// let output = scanf!("2 false fox", char::is_whitespace, u8, bool, String);
/// assert_eq!((Some(2), Some(false), Some("fox")), output);
/// ```
macro_rules! scanf {
    ( $string:expr, $sep:expr, $( $x:ty ),+ ) => {{
        let mut iter = $string.split($sep);
        ($(iter.next().and_then(|word| word.parse::<$x>().ok()),)*)
    }}
}

/// Initialise the SquareGeometry strcuture by reading the map from a file.
/// # Examples
/// ```
/// use std::path::Path;
/// let map = from_file(Path::new("map_test.txt"));
/// ```
fn from_file(path: &Path) -> SquareGeometry {
    let lines = match lines_from_file(path) {
        Err(why) => panic!("Problem with file '{}': {:?}", path.display(), why),
        Ok(x) => x,
    };

    let mut map = SquareGeometry{map_w:0, length:0, map_h:0, map: Vec::new(), blocked:0};

    for l in lines.enumerate() {
        let motif = l.1.unwrap();
        if l.0 == 0 {
            let tmp = scanf!(motif, char::is_whitespace, usize, usize);
            map.map_w = tmp.0.unwrap();
            map.map_h = tmp.1.unwrap();
            map.length = map.map_h * map.map_w;
            continue;
        }

        for c in motif.split(char::is_whitespace) {
            map.map.push(
                c.parse::<u8>().unwrap()
            );
        }
    }

    return map;
}

fn main() {
    let mut fname:String = "wrong".to_string();
    {
        let mut ap = ArgumentParser::new();
        ap.set_description("Testing program for pathfinding.");
        ap.refer(&mut fname).add_argument("FileName", Store, "File name containing the map.");
        ap.parse_args_or_exit();
    }

    let path = Path::new(&fname);
    let map = from_file(path);

    let res = match breadth_first_search(0, 0, 9, 4, &map) {
        Err(why) => panic!("Path finding failed: '{}'", why),
        Ok(x) => x,
    };

    println!("{:?}", res);
}
