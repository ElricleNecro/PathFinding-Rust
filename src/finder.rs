use graph::{Graph,MapGeometry};

// Kind of a priority Queue:
// use std::collections::BinaryHeap;
use std::collections::VecDeque;

// A trait to follow to implements error for the Result type:
// use std::error::Error;
// The Result type, classically used to differenciate error from good answer:
use std::result::Result;

// #[derive(Debug)]
// struct FailedPathFinding {
    // reason: &'static str,
// }

// impl Error for FailedPathFinding {
    // fn description(&self) -> &str {
        // self.reason
    // }
// }

/// Implementation of the breadth first algorithm.
///
/// It take 2 (x, y) coordinate: one as start, another as target
pub fn breadth_first_search<T: MapGeometry>(n_start_x: isize, n_start_y: isize, n_target_x: isize, n_target_y: isize, map: &T) -> Result<Vec<(isize,isize)>, &'static str> {
    if !map.contain(n_target_x, n_target_y) {
        return Err("target is not on the map");
    }

    if n_start_x == n_target_x && n_start_y == n_target_y {
        return Err("starting point and target are identical");
    }

    let mut graph: Graph = Graph::new(map.len());
    graph.create(map);

    let mut frontier = VecDeque::new();
    frontier.push_back(map.to_flatten_idx(n_start_x, n_start_y));

    let mut came_from: Vec<isize> = vec![-2isize; map.len()]; //Vec::new();
    // came_from.resize(map_w * map_h, -2isize);
    came_from[map.to_flatten_idx(n_start_x, n_start_y)] = -1;

    // Now we are looping over our frontier:
    let goal = map.to_flatten_idx(n_target_x, n_target_y);
    while !frontier.is_empty() {
        // Getting back the first in the queue, removing it in the same time:
        let current = frontier.pop_front().unwrap();

        // If we have our target, no need to go futher:
        if current == goal {
            break;
        }

        // Adding neighbor to the frontier:
        for v in graph.get_neighbor_of(current) {
            if came_from[*v] == -2 {
                frontier.push_back(*v);
                came_from[*v] = current as isize;
            }
        }
    }

    let mut result: Vec<(isize,isize)> = Vec::new();
    let mut current: isize = came_from[map.to_flatten_idx(n_target_x, n_target_y)];
    while current != -1 {
        result.push(map.from_flatten_idx(current as usize));
        current = came_from[current as usize];
    }

    result.reverse();

    return Ok(result);
}

