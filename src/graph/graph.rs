// use std::ops::Index;
use std::slice::Iter;

use super::node::Node;

pub struct Graph {
    node_lst: Vec<Node>,
    length: usize,
}

pub trait MapGeometry {
    fn to_flatten_idx(&self, isize, isize) -> usize;
    fn from_flatten_idx(&self, usize) -> (isize, isize);

    fn contain(&self, isize, isize) -> bool;

    fn neighbors_direction(&self) -> Vec<(isize,isize)>;

    fn len(&self) -> usize;

    // iterator over direction:
    // fn iter() -> Iterator;
}

impl Graph {
    pub fn new(length: usize) -> Graph {
        let mut res = Graph {
            node_lst: Vec::new(),

            length: length,
        };
        for _ in 0..res.length {
            res.node_lst.push(Node::new(0isize, 0isize, 0usize));
        }

        res
    }

    pub fn with_max_neighbor(length: usize, max_nb: usize) -> Graph {
        let mut res = Graph {
            node_lst: Vec::new(),

            length: length,
        };
        for _ in 0..res.length {
            res.node_lst.push(Node::with_max_neighbor(0isize, 0isize, 0usize, max_nb));
        }

        res
    }

    pub fn create<T: MapGeometry>(&mut self, geom: &T) {
        for i in 0..self.length {
            let (x, y) = geom.from_flatten_idx(i);
            self.node_lst[i].x = x;
            self.node_lst[i].y = y;

            self.node_lst[i].id = i;

            self.init_neighbors(i, geom);
        }
    }

    fn init_neighbors<G: MapGeometry>(&mut self, node: usize, geom: &G) {
        let mut x:isize;
        let mut y:isize;

        for (v, w) in geom.neighbors_direction() {
            x = self.node_lst[node].x + v;
            y = self.node_lst[node].y + w;

            if geom.contain(x, y) {
                self.node_lst[node].neighbor.push(geom.to_flatten_idx(x, y));
            }
        }
    }

    pub fn get_neighbor_of(&self, idx: usize) -> Iter<usize> {
        self.node_lst[idx].get_neighbors_as_iter()
    }
}

