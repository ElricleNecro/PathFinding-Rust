// use std::cmp::Ord;
use std::slice::Iter;

pub struct NeighborVector {
    nb_neighbor: usize,
    eff_neighbor: usize,

    neighbor: Vec<usize>,
}

impl NeighborVector {
    pub fn new(nb_max: usize) -> NeighborVector {
        NeighborVector {
            nb_neighbor: nb_max,
            eff_neighbor: 0usize,
            neighbor: Vec::with_capacity(nb_max),
        }
    }

    pub fn push(&mut self, idx:usize) -> bool {
        if self.eff_neighbor >= self.nb_neighbor {
            return false;
        }

        self.neighbor.push(idx);
        self.eff_neighbor += 1;
        return true;
    }
}

pub struct Node {
    pub x: isize,
    pub y: isize,
    pub id: usize,

    pub neighbor: NeighborVector,
}

impl Node {
    pub fn new(x:isize, y:isize, id:usize) -> Node  {
        Node {
            x: x, y:y, id:id,

            neighbor:NeighborVector::new(4usize)
        }
    }

    pub fn with_max_neighbor(x:isize, y:isize, id:usize, nb_neighbor:usize) -> Node  {
        Node {
            x: x, y:y, id:id,

            neighbor:NeighborVector::new(nb_neighbor)
        }
    }

    pub fn get_neighbors_as_iter(&self) -> Iter<usize> {
        self.neighbor.neighbor.iter()
    }
}
