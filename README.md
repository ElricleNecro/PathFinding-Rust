# Rust Path Finding
===================
## Intro speech
Welcome to my path finding library in Rust. This library will evolve in parallel
of my c++ equivalent, for educational purpose.

Right now, only a Breadth first algorithm is implemented. A Djikstra will come
soon. Maybe an A* will follow, who knows!

## How to compile
It's easy: install rust and cargo on your GNU/Linux distribution, clone the
repository, and finish by
```
cargo build --release
```

## Supported platform
As of today, nothing forbid it to work under Mac OS X and Microsoft Windows. But
this may change in the future, depending of my will, rust will, and what I end up
using this library.